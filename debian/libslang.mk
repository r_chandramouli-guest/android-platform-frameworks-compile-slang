include /usr/include/android/arch/android_includes.mk
include slang/rs_version.mk
include debian/use_llvm.mk

NAME = libslang
SOURCES = \
	slang.cpp	\
	slang_utils.cpp	\
	slang_backend.cpp	\
	slang_pragma_recorder.cpp	\
	slang_diagnostic_buffer.cpp
OBJECTS = $(SOURCES:.cpp=.o)
CXXFLAGS += -fPIC -c $(RS_VERSION_DEFINE) -D__HOST__ -Wno-sign-promo -Wall \
            -Wno-unused-parameter -Wno-return-type -Werror \
            -DTARGET_BUILD_VARIANT=$(TARGET_BUILD_VARIANT)
CPPFLAGS += $(ANDROID_INCLUDES) -I/usr/include/android -Iinclude
LDFLAGS += -ldl -lpthread -lLLVMWriter_2_9 -lLLVMWriter_2_9_func -lLLVMWriter_3_2 \
           -lStripUnkAttr

build: $(OBJECTS)
	ar rs $(NAME).a $^

clean:
	rm -f *.a *.o

$(OBJECTS): %.o: %.cpp
	c++ $< -o $@ $(CXXFLAGS) $(CPPFLAGS)
