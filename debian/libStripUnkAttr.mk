include /usr/include/android/arch/android_includes.mk
include slang/rs_version.mk
include debian/use_llvm.mk

NAME = libStripUnkAttr
SOURCES =  strip_unknown_attributes.cpp \
           strip_unknown_attributes_pass.cpp
OBJECTS = $(SOURCES:.cpp=.o)
CXXFLAGS += -fPIC -c $(RS_VERSION_DEFINE) -Islang
CPPFLAGS += $(ANDROID_INCLUDES) -I/usr/include/android -Iinclude

build: $(OBJECTS)
	ar rs $(NAME).a $^

clean:
	rm -f *.a *.o

$(OBJECTS): %.o: %.cpp
	c++ $< -o $@ $(CXXFLAGS) $(CPPFLAGS)
